@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-0 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body text-center">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Anda Masuk Sebagai {{ Auth::user()->name }}!
                    
				<h2>TABUNGANKU</h2>

				<p></p>
				<hr>
				<h4>Sisa Uang Anda Saat ini:</h4>
				<h5><?php echo $total['currency']; ?>. <?php echo $total['totalUang']; ?></h5>
			
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
