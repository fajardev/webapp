@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
					<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<form class="form-horizontal" name="formCreateTabungan" action="<?php echo URL::to('tabungan/proses')?>" method="POST">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				 		<div class="form-group">
						    <label for="inputname" class="col-sm-2 control-label">Nama Tabungan</label>
						    <div class="col-sm-10">
					      		<input type="text" name="name" class="form-control" placeholder="Nama Tabungan" required="true">
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputdeskripsi" class="col-sm-2 control-label">Deskripsi Tabungan</label>
						    <div class="col-sm-10">
					      		<input type="text" name="description" class="form-control" placeholder="Deskripsi Tabungan" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputnominal" class="col-sm-2 control-label">Nominal</label>
						    <div class="col-sm-10">
					      		<input type="text" name="nominal_save" class="form-control" placeholder="Nominal Tabungan" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      	<button type="submit" class="btn btn-success">Submit</button>
						      	<a href="{{URL::to('/tabungan')}}" class="btn btn-default">Kembali</a>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
