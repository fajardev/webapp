@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
					<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<form class="form-horizontal" name="formCreatePengeluaran" action="<?php echo URL::to('pengeluaran/proses')?>" method="POST">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				 		<div class="form-group">
						    <label for="inputname" class="col-sm-2 control-label">Nama Pengeluaran</label>
						    <div class="col-sm-10">
					      		<input type="text" name="name" class="form-control" placeholder="Nama Pengeluaran" required="true">
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputdeskripsi" class="col-sm-2 control-label">Deskripsi Pengeluaran</label>
						    <div class="col-sm-10">
					      		<input type="text" name="description" class="form-control" placeholder="Deskripsi Pengeluaran" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputnominal" class="col-sm-2 control-label">Nominal</label>
						    <div class="col-sm-10">
					      		<input type="text" name="nominal" class="form-control" placeholder="Nominal Pengeluaran" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      	<button type="submit" class="btn btn-success">Submit</button>
						      	<a href="{{URL::to('/pengeluaran')}}" class="btn btn-default">Kembali</a>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
