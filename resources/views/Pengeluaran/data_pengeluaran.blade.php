@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-0 col-md-offset-0">
        <div class="jumbotron text-center">
    <h2>PENGELUARANKU</h2>

    <p></p>
    <hr>
    <h4>Pengeluaran Anda Saat ini:</h4>
    <h5><?php echo $pengeluaran['currency']; ?>. <?php echo $pengeluaran['totalPengeluaran']; ?></h5>
</div>
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
				<a href="{{URL::to('pengeluaran/create')}}" class="btn btn-success btn-sm" style="margin-bottom:20px;">TAMBAH PENGELUARAN</a>
				<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<div class="table-responsive">
                        <table class="display table table-bordered" id="example">
							<thead>
								<tr>
									<th>Id</th>
									<th>Pengeluaran</th>
									<th>Deskripsi Pengeluaran</th>
									<th>Nominal Pengeluaran</th>
									<th>Tanggal Pengeluaran</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $count=1; ?>
									@foreach($list as $row)
								<tr>
									<td><?php echo $count ?></td>
									<td><?php echo $row->name ?></td>
									<td><?php echo $row->description?></td>
									<td>IDR. <?php echo number_format($row->nominal,2,',','.')?></td>
									<td><?php echo $row->created_at?></td>
									<td>
										<a href="{{URL::to('pengeluaran/delete')}}/{{$row->id}}" class="btn btn-danger btn-sm">Delete</a>
									</td>
								</tr>
								<?php $count++; ?>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
