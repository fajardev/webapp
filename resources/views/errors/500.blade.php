<div class="content">
    <div class="title">Something went wrong.</div>

    @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
        <div class="subtitle">Error ID: {{ Sentry::getLastEventID() }}</div>

        <!-- Sentry JS SDK 2.1.+ required -->
        <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

        <script>
            Raven.showReportDialog({
                eventId: '{{ Sentry::getLastEventID() }}',
                // use the public DSN (dont include your secret!)
                dsn: 'https://b7857d3a504b4508b832d46966c2378b@sentry.io/297207',
                user: {
                    'name': 'Fajar Hidayatulloh',
                    'email': 'fajarhidayatulloh06@gmail.com',
                }
            });
        </script>
    @endif
</div>