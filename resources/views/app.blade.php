<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="<?php echo URL::to('css/app.css')?>" rel="stylesheet">
	<link href="<?php echo URL::to('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet">
	<link href="<?php echo URL::to('assets/css/jquery.ui.css')?>" rel="stylesheet">
	<!--<link rel="stylesheet" href="<?php echo URL::to('assets/css/bootstrap.min.css') ?>">-->
	<link rel="stylesheet" href="<?php echo URL::to('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" href="<?php echo URL::to('assets/css/bootstrapValidator.min.css') ?>">
	<script src="<?php echo URL::to('assets/js/jquery.dataTables.js')?>"></script>
    
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">TABUNGANKU</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{URL::to('/login')}}">Login</a></li>
						<li><a href="{{URL::to('/register')}}">Register</a></li>
					@else
						<li><a href="{{URL::to('/profile')}}">{{ Auth::user()->name }}</a>	
						<li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
					
				</ul>
				<ul class="nav navbar-nav">
					<li><a href="{{URL::to('/home')}}">Home</a></li>
					<li><a href="{{URL::to('/tabungan')}}">Tabungan</a></li>
					<li><a href="{{URL::to('/pemasukan')}}">Pemasukan</a></li>
					<li><a href="{{URL::to('/pengeluaran')}}">Pengeluaran</a></li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	 <div class="container">
        
        @yield('content')
           
		<br><br>
		<p>Created Fajar Hidayatulloh &copy; 2018</p>
    </div>

	
    <script src="<?php echo URL::to('js/angular.min.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/jquery-2.1.1.min.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/jquery.min.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/bootstrapvalidator.min.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/FormValidation.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/bootstrap.js') ?>"></script>
	<script src="<?php echo URL::to('assets/js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript   ">
        $(document).ready( function () {
            $('#example').DataTable({         
	          	"language": {
		            "url":"http://cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
		            "sEmptyTable":"Tidak ada data di database",
		            "sSearch":"Cari:",
		            "sInfo":"Menampilkan _START_ sampai _END_ dari _TOTAL_ entri"
	        	}
        	});
        });
    </script>
</body>
</html>

