@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
					<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<form class="form-horizontal" action="<?php echo URL::to('review-buku/update')?>/{{$reg->id}}" method="POST">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				 	<input type="hidden" name="id" value="{{ $reg->id }}">
				 		<div class="form-group">
						    <label for="inputjudul" class="col-sm-2 control-label">Judul Buku</label>
						    <div class="col-sm-10">
					      		<input type="text" name="judul" class="form-control" placeholder="Judul Buku" value="{{ $reg->judul }}" required="true">
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputpenulis" class="col-sm-2 control-label">Penulis Buku</label>
						    <div class="col-sm-10">
					      		<input type="text" name="penulis" class="form-control" placeholder="Penulis Buku" value="{{ $reg->penulis }}" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputpenerbit" class="col-sm-2 control-label">Penerbit</label>
						    <div class="col-sm-10">
					      		<input type="text" name="penerbit" class="form-control" placeholder="Penerbit" value="{{ $reg->penerbit }}" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputtglterbit" class="col-sm-2 control-label">Tanggal Terbit Buku</label>
						    <div class="col-sm-10">
					      		<input type="text" name="tgl_terbit" class="form-control" placeholder="Tanggal Terbit : contoh: 12 Desember 2017" value="{{ $reg->tgl_terbit }}" required>
					    	</div>
					  	</div>
					  	
					  	<div class="form-group">
						    <label for="inputisi" class="col-sm-2 control-label">Resensi Buku</label>
						    <div class="col-sm-10">
					      		<textarea name="isi" class="form-control" placeholder="Resensi Buku" required>{{ $reg->isi }}</textarea>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputtebalbuku" class="col-sm-2 control-label">Tebal Buku/Tebal Halaman</label>
						    <div class="col-sm-10">
					      		<input type="text" name="tebel_buku" class="form-control" placeholder="Tebal Buku/Tebal Halaman" value="{{ $reg->tebel_buku }}" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputdimensi" class="col-sm-2 control-label">Dimensi Buku</label>
						    <div class="col-sm-10">
					      		<input type="text" name="dimensi" class="form-control" placeholder="Dimensi Buku : contoh: 13cm x 14cm" value="{{ $reg->dimensi }}" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputharga" class="col-sm-2 control-label">Harga Buku</label>
						    <div class="col-sm-10">
					      		<input type="text" name="harga" class="form-control" placeholder="Harga Buku" value="{{ $reg->harga }}" required>
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <label for="inputcover" class="col-sm-2 control-label">Cover Buku</label>
						    <div class="col-sm-10">
					      		<input type="file" name="image" class="form-control" value="{{ $reg->filePath}}">
					      		<img src="<?php echo URL::to('images/'.$reg->filePath)?>" width="80" height="50" alt="{{$reg->judul}}">
					    	</div>
					  	</div>

					  	<div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      	<button type="submit" class="btn btn-primary">Update</button>
						      	<a href="{{URL::to('/review-buku')}}" class="btn btn-default">Kembali</a>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
