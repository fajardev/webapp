@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-0 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
				<a href="{{URL::to('review-buku/create')}}" class="btn btn-primary btn-sm" style="margin-bottom:20px;">CREATE NEW REVIEW BUKU</a>
				<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<div class="table-responsive">
                        <table class="display table table-bordered" id="example">
							<thead>
								<tr>
									<th>Id</th>
									<th>Judul</th>
									<th>Penulis</th>
									<th>Penerbit</th>
									<th>Tanggal Terbit</th>
									<th>Resensi</th>
									<th>Tebal Buku</th>
									<th>Dimensi</th>
									<th>Harga</th>
									<th>Cover</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $count=1; ?>
									@foreach($buku as $reg)
								<tr>
									<td><?php echo $count ?></td>
									<td><?php echo $reg->judul ?></td>
									<td><?php echo $reg->penulis?></td>
									<td><?php echo $reg->penerbit?></td>
									<td><?php echo $reg->tgl_terbit?></td>
									<td><?php echo $reg->isi?></td>
									<td><?php echo $reg->tebel_buku?></td>
									<td><?php echo $reg->dimensi?></td>
									<td>Rp. <?php echo number_format($reg->harga)?></td>
									<td>
										<img src="<?php echo URL::to('images/'.$reg->filePath)?>" width="80" height="50" alt="{{$reg->judul}}">
									</td>
									<td>
										<a href="{{URL::to('review-buku/edit')}}/{{$reg->id}}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{URL::to('review-buku/delete')}}/{{$reg->id}}" class="btn btn-danger btn-sm">Delete</a>
									</td>
								</tr>
								<?php $count++; ?>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
