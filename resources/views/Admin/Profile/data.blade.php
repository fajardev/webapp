@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
					<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<form class="form-horizontal" name="formUsers" action="<?php echo URL::to('user/update/')?>" method="POST">
				 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				 	<input type="hidden" name="id" class="form-control" value="{{Auth::user()->id}}">
				 		<div class="form-group">
						    <label for="inputNama" class="col-sm-2 control-label">Nama Lengkap</label>
						    <div class="col-sm-10">
					      		<input type="text" name="name" class="form-control" value="{{Auth::user()->name}}">
					    	</div>
					  	</div>
					  	<div class="form-group">
						    <label for="inputUsername" class="col-sm-2 control-label">Username</label>
						    <div class="col-sm-10">
					      		<input type="text" name="username" class="form-control" value="{{Auth::user()->username}}">
					    	</div>
					  	</div>
					  	<div class="form-group">
						    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
						    <div class="col-sm-10">
					      		<input type="email" name="email" class="form-control" value="{{Auth::user()->email}}">
					    	</div>
					  	</div>
					  	
					  	<div class="form-group">
						    <div class="col-sm-offset-2 col-sm-10">
						      	<button type="submit" class="btn btn-primary">Update</button>
						      	<a href="{{URL::to('/user')}}" class="btn btn-default">Kembali</a>
						    </div>
					  	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
