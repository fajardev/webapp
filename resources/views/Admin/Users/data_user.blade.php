@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{$title}}</div>

				<div class="panel-body">
				<a href="{{URL::to('user/create')}}" class="btn btn-primary btn-sm" style="margin-bottom:20px;">CREATE NEW USER</a>
				<center><p class="bg-success">{{Session::get('message')}}</p></center>
				 	<div class="table-responsive">
                        <table class="display table table-bordered" id="example">
							<thead>
								<tr>
									<th>Id</th>
									<th>Full Name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $count=1; ?>
									@foreach($user as $reg)
								<tr>
									<td><?php echo $count ?></td>
									<td><?php echo $reg->name ?></td>
									<td><?php echo $reg->username?></td>
									<td><?php echo $reg->email?></td>
									<td>
										<a href="{{URL::to('user/edit')}}/{{$reg->id}}" class="btn btn-primary btn-sm">Edit</a>
										<a href="{{URL::to('user/delete')}}/{{$reg->id}}" class="btn btn-danger btn-sm">Delete</a>
									</td>
								</tr>
								<?php $count++; ?>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
