<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



require app_path(). '/Modules/User/UserRoutes.php'; //route User
require app_path(). '/Modules/FrontEnd/FrontendRoutes.php'; //route FrontEnd
require app_path(). '/Modules/Auth/AuthRoutes.php'; //route Authentication
require app_path(). '/Modules/Home/HomeRoutes.php'; //route Home
require app_path(). '/Modules/Buku/BukuRoutes.php'; //route Buku
require app_path(). '/Modules/Profile/ProfileRoutes.php'; //route Profile
require app_path(). '/Modules/Tabungan/TabunganRoutes.php'; //route Profile
require app_path(). '/Modules/Pengeluaran/PengeluaranRoutes.php'; //route Profile
require app_path(). '/Modules/Pemasukan/PemasukanRoutes.php'; //route Profile

Auth::routes();


