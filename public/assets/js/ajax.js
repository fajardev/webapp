
/* form validation */
$(document).ready (function() {
	
    $("form[name='formUsers']").bootstrapValidator({
      	feedbackIcons: {
       	    valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
      	},
      
      	fields: {
           	name: {
            	validators: {
             		notEmpty: {
             			message: 'Nama Tidak Boleh Kosong!!!'
             		},
             		regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Nama Hanya Boleh Mengandung Huruf.'
                    }
           		}
           	},
            username: {
                validators: {
                    notEmpty: {
                        message: 'Username Tidak Boleh Kosong!!!'
                    }
                }
            },
           	
           	email: {
        	    validators: {
        	       notEmpty: {
                        message: 'Email Tidak Boleh Kosong!!!'
                    },
        	    	emailAddress: {
        	    		message: 'Email Tidak Valid'
        	     	}
        	    }
            },
           	
           	password: {
        	    validators: {
        	    	notEmpty: {
        	      		message: 'Password Tidak Boleh Kosong!!!'
        	     	},
        	    }
           	},
        }
    })
});


