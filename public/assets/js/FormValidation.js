
/* form validation Create New User*/
$(document).ready (function() {
	
    $("form[name='formUsers']").bootstrapValidator({
      	feedbackIcons: {
       	    valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
      	},
      
      	fields: {
           	name: {
            	validators: {
                    stringLength: {
                        min: 2,
                        message:'Tidak valid'
                    },
             		notEmpty: {
             			message: 'Nama Tidak Boleh Kosong!!!'
             		},
             		regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Nama Hanya Boleh Mengandung Huruf.'
                    }
           		}
           	},
            username: {
                validators: {
                    stringLength: {
                        min: 4,
                        message:'Tidak valid'
                    },
                    notEmpty: {
                        message: 'Username Tidak Boleh Kosong!!!'
                    }
                }
            },
           	
           	email: {
        	    validators: {
        	       notEmpty: {
                        message: 'Email Tidak Boleh Kosong!!!'
                    },
        	    	emailAddress: {
        	    		message: 'Email Tidak Valid'
        	     	}
        	    }
            },
           	
           	password: {
        	    validators: {
                    stringLength: {
                        min: 4,
                        message:'Tidak valid'
                    },

        	    	notEmpty: {
        	      		message: 'Password Tidak Boleh Kosong!!!'
        	     	}
        	    }
           	},

            password_confirmation: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Password Tidak Sama'
                    }
                }
            }
        }
    })
});


/* form validation */
$(document).ready (function() {
  
    $("form[name='formLogin']").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
      
        fields: {
            
            email: {
              validators: {
                 notEmpty: {
                        message: 'Email Tidak Boleh Kosong!!!'
                    },
                emailAddress: {
                  message: 'Email Tidak Valid'
                }
              }
            },
            
            password: {
              validators: {
                    stringLength: {
                        min: 4,
                        message:'Tidak valid'
                    },
                notEmpty: {
                    message: 'Password Tidak Boleh Kosong!!!'
                },
              }
            },
        }
    })
});

/* form validation */
$(document).ready (function() {
  
    $("form[name='formCreateTabungan']").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
      
        fields: {
            
            name: {
              validators: {
                 notEmpty: {
                        message: 'Nama Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },

            description: {
              validators: {
                 notEmpty: {
                        message: 'Deskripsi Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },
            
            nomial_save: {
              validators: {
                 notEmpty: {
                        message: 'Nominal Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },

        }
    })
});

/* form validation */
$(document).ready (function() {
  
    $("form[name='formCreateTabungan']").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
      
        fields: {
            
            name: {
              validators: {
                 notEmpty: {
                        message: 'Nama Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },

            description: {
              validators: {
                 notEmpty: {
                        message: 'Deskripsi Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },
            
            nominal_save: {
              validators: {
                 notEmpty: {
                        message: 'Nominal Tabungan Tidak Boleh Kosong!!!'
                    }
                }
            },
        }
    })
});

/* form validation */
$(document).ready (function() {
  
    $("form[name='formCreatePengeluaran']").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
      
        fields: {
            
            name: {
              validators: {
                 notEmpty: {
                        message: 'Nama Pengeluaran Tidak Boleh Kosong!!!'
                    }
                }
            },

            description: {
              validators: {
                 notEmpty: {
                        message: 'Deskripsi Pengeluaran Tidak Boleh Kosong!!!'
                    }
                }
            },

            nominal: {
                validators: {
                   notEmpty: {
                          message: 'Nominal Pengeluaran Tidak Boleh Kosong!!!'
                      }
                  }
              }

        }
    })
});

/* form validation */
$(document).ready (function() {
  
    $("form[name='formCreatePemasukan']").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
      
        fields: {
            
            name: {
              validators: {
                 notEmpty: {
                        message: 'Nama Pemasukan Tidak Boleh Kosong!!!'
                    }
                }
            },

            description: {
              validators: {
                 notEmpty: {
                        message: 'Deskripsi Pemasukan Tidak Boleh Kosong!!!'
                    }
                }
            },

            nominal: {
                validators: {
                   notEmpty: {
                          message: 'Nominal Pemasukan Tidak Boleh Kosong!!!'
                      }
                  }
              }

        }
    })
});



