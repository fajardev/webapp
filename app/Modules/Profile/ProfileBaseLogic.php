<?php  
namespace App\Modules\Profile;

use Illuminate\Http\Request;
use App\Models\UserModel;

	class ProfileBaseLogic{

		protected $request;
		protected $user;

		public function __construct(Request $request, UserModel $user){
			$this->request=$request;
			$this->user=$user;
		}
		
	}

?>