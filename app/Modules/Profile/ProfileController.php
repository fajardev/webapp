<?php  
namespace App\Modules\Profile;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Profile\ProfileConstant as Message;
use Session;
	
	class ProfileController extends BaseController{

		public function __construct(){
			$this->middleware('auth');
		}

		public function index(){
			$data['title']='';
			return response()->view('Admin.Profile.data',$data);
		}

	}

?>