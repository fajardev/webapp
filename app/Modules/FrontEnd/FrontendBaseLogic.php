<?php  
namespace App\Modules\FrontEnd;

use Illuminate\Http\Request;
use App\Models\ArtikelModel;
use App\Modules\FrontEnd\FrontendConst as Message;

class FrontendBaseLogic{
	protected $artikel;

	public function __construct(ArtikelModel $artikel){
		$this->artikel=$artikel;
	}

	/**
	 * [getArtikelList]
	 * 
	 * @return [type] [description]
	 */
	public function getArtikelList(){
		$artikel=$this->artikel->all();
		if($artikel==NULL){
			return response()->json([
				'error'=>[Message::DATA_ERROR_MSG]
			],400);
		}else{
			return $artikel;
		}
		
	}

	public function getTitleArtikel($id){
		$artikel=$this->artikel->find($id);
		return $artikel;
	}

	public function getReadArtikel($id){
		$artikel=$this->artikel->find($id);
		if(!$artikel){
			return response()->json([
				'error'=>[Message::READ_ERROR_MSG]
			],400);
		}else{
			return $artikel;
		}
	}
}
?>