<?php  
namespace App\Modules\FrontEnd;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\FrontEnd\FrontendBaseLogic as BaseLogic;

class FrontendLogic{
	protected $baseLogic;

	public function __construct(BaseLogic $baseLogic){
		$this->baseLogic=$baseLogic;
	}

	public function getList($request){
		$artikel=$this->baseLogic->getArtikelList($request);
		return $artikel;

	}

	public function getTitle($request){
		$artikel=$this->baseLogic->getReadArtikel($request);
		return $artikel;
	}

	public function getRead($request){
		$artikel=$this->baseLogic->getReadArtikel($request);
		return $artikel;
		
	}
}

?>