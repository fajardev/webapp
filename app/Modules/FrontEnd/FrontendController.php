<?php  

namespace App\Modules\FrontEnd;

use App\Models\ArtikelModel;
use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\FrontEnd\FrontendConst as Message;

class FrontendController extends BaseController{

	protected $request;
	protected $frontendLogic;
	public function __construct(Request $request, FrontendLogic $frontendLogic){
		$this->request=$request;
		$this->frontendLogic=$frontendLogic;
	}
	function index(){
		//$data['title']	= Message::TITLE;
		$request=$this->request->all();
		$data= $this->frontendLogic->getList($request);
		return response()->json($data);
		//$data['artikel']= $this->frontendLogic->getList($request);
		//return response()->view('FrontEnd.index',$data);
	}

	function read($id){
		//$data=$this->frontendLogic->getTitle($id)->judul;
        $data=$this->frontendLogic->getRead($id);
        //return response()->view('FrontEnd.show',$data);
        
        return $data;
	}
	
}

?>