<?php

namespace App\Modules\Home;

use Illuminate\Http\Request;
use App\Modules\Home\HomeBaseLogic as BaseLogic;

use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use App\Modules\Common\Exception\ValidationFailException;

use App\Modules\Common\Response\ApiResponse;
use App\Modules\Common\Constant\ApiConstant;
use App\Modules\Common\Constant\HttpConstant as HTTP;

class HomeLogic{

    protected $baseLogic;
    protected $response;

    function __construct(BaseLogic $baseLogic, ApiResponse $response){
        $this->baseLogic = $baseLogic;
        $this->response = $response;
    }

    public function getAllHome()
    {
        
        try 
        {

            $home = $this->baseLogic->home();

            $this->response->setStatus(ApiConstant::SUCCESS_CD);
            $this->response->setData($home);
            $this->response->setHttpCd(HTTP::HTTP_OK);

        } 
        catch(ValidationFailException $e) {
            $this->response->setStatus(ApiConstant::ERROR_CD);
            $this->response->setMessageCd($e->getCode());
            $this->response->setMessage($e->getMessage());
            $this->response->setHttpCd(HTTP::HTTP_UNAUTHORIZED);
            $this->response->setDetail($e->getMessageBag());

        } 
        catch(DatabaseProblemException $e) {
            $this->response->setStatus(ApiConstant::ERROR_CD);
            $this->response->setMessageCd($e->getCode());
            $this->response->setMessage($e->getMessage());
            $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);

        } 
        catch(\Exception $e) {
            $this->response->setStatus(ApiConstant::ERROR_CD);
            $this->response->setMessageCd($e->getCode());
            $this->response->setMessage($e->getMessage());
            $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->response;
    }

    public function getApiStatus()
    {
        try 
        {

            $ApiStatus = $this->baseLogic->setApiStatus();

            $this->response->setStatus(ApiConstant::SUCCESS_CD);
            $this->response->setData($ApiStatus);
            $this->response->setHttpCd(HTTP::HTTP_OK);

        } 
        catch(DatabaseProblemException $e) {
            $this->response->setStatus(ApiConstant::ERROR_CD);
            $this->response->setMessageCd($e->getCode());
            $this->response->setMessage($e->getMessage());
            $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);

        } 
        catch(\Exception $e) {
            $this->response->setStatus(ApiConstant::ERROR_CD);
            $this->response->setMessageCd($e->getCode());
            $this->response->setMessage($e->getMessage());
            $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->response;
    
    }

    public function getTotal()
    {
        $total = $this->baseLogic->setTotal();
        return $total;
        // try 
        // {

        //     $total = $this->baseLogic->setTotal();

        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($total);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);

        // } 
        // catch(ValidationFailException $e) {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_UNAUTHORIZED);
        //     $this->response->setDetail($e->getMessageBag());

        // } 
        // catch(DatabaseProblemException $e) {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);

        // } 
        // catch(\Exception $e) {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }

        // return $this->response;
    }
}