<?php

namespace App\Modules\Home;

use Illuminate\Http\Request;
use DB;
use Cache;

class HomeBaseLogic{

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:24:29 
     * @Desc: in home page, user can see sum money right now 
     */    
    public function setTotal()
    {   
        try {

            $pemasukan = DB::connection('tabungan')->table('pemasukan')->select('nominal')->sum('nominal')->where('cif_id', \Auth::user()->id);
            $pengeluaran = DB::connection('tabungan')->table('pengeluaran')->select('nominal')->sum('nominal')->where('cif_id', \Auth::user()->id);

            $data = [
                'totalUang' => number_format($pemasukan - $pengeluaran , 2,',','.'),
                'currency'  =>'IDR',

            ];
            return $data;

        } catch(\Exception $e) {
            return false;
        }
    }
}
