<?php  
namespace App\Modules\Home;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Home\HomeLogic as Home;
use App\Modules\Home\HomeConstant;
use Session;

/** 
 * @Author: fajar 
 * @Date: 2018-03-27 16:27:43 
 * @Desc:  
 */
class HomeController extends BaseController{

	protected $home;
    protected $request;
    
	public function __construct(
		Home $home, 
		Request $request
	)
	{
		$this->home = $home;
        $this->request = $request;
		//$this->middleware('guest');
	}

	/** 
	 * @Author: fajar 
	 * @Date: 2018-03-27 16:26:52 
	 * @Desc: in home page, user can see sum money right now  
	 */	
	public function index()
	{
		try {

			$req = $this->request->all();
			$data['title'] = Message::TITLE_HOME;
			$data['total'] = $this->home->getTotal();
			return response()->view('',$data);

		} catch(\Exception $e) {
			return response()->view('errors');
		}
	}
}

?>