<?php

Route::group(['namespace' => 'Pengeluaran', 'prefix' => 'pengeluaran','middleware' => 'auth'], function () 
{   
    Route::get('/','PengeluaranController@Pengeluaran'); 
    Route::get('/create', 'PengeluaranController@create');
    Route::post('/proses', 'PengeluaranController@store');
    Route::get('/delete/{id}', 'PengeluaranController@delete');
});