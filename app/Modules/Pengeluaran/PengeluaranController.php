<?php
namespace App\Modules\Pengeluaran;

use App\Modules\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Modules\Pengeluaran\PengeluaranLogic as Pengeluaran;
use App\Modules\Pengeluaran\PengeluaranConstant as Message;
use Session;

/** 
 * @Author: fajar 
 * @Date: 2018-03-27 16:00:10 
 * @Desc:  
 */

class PengeluaranController extends BaseController
{
    protected $pengeluaran;
    protected $request;
    function __construct(
        Pengeluaran $pengeluaran, 
        Request $request
    )
    {
        $this->pengeluaran = $pengeluaran;
        $this->request = $request;
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:07:40 
     * @Desc: lisf of pengeluaran 
     */    
    public function Pengeluaran()
    {
        try{
            $req = $this->request->all();
            $response['pengeluaran'] = $this->pengeluaran->getPengeluaran($req);
            $response['list'] = $this->pengeluaran->getListPengeluaran($req);
            $response['title'] = 'Pengeluaran Anda.';
            return response()->view('Pengeluaran.data_pengeluaran',$response);
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }

    public function ListPengeluaran()
    {
        $req = $this->request->all();
        $response = $this->pengeluaran->getListPengeluaran($req);
        return response()->json($response->transform(), $response->httpCd())->header('Access-Control-Allow-Origin', '*');
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:07:56 
     * @Desc: page of input pengeluaran
     */    
    public function create()
    {
        try {
            $res['title'] = Message::CREATE_NEW_PENGELUARAN_MSG;
            return response()->view('Pengeluaran.create_pengeluaran',$res);
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:08:21 
     * @Desc: process of pengeluaran 
     */    
    public function store()
    {
        try {
            $request=$this->request->all();
            $data=$this->pengeluaran->getCreatePengeluaran($request);
            Session::flash('message',Message::CREATE_NEW_PENGELUARAN_SUCCESS_MSG);
            return redirect('pengeluaran/create');
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:08:40 
     * @Desc: delete list of pengeluaran 
     */    
    public function delete($id){
        try {
            $pengeluaran=$this->pengeluaran->delete($id);
            Session::flash('message', Message::DELETE_PENGELUARAN_SUCCESS);
            return redirect('/pengeluaran');
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }
    
}