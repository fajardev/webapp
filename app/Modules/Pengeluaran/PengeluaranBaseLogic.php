<?php
namespace App\Modules\Pengeluaran;

use Illuminate\Http\Request;
use App\Modules\Transformers\PengeluaranTransformer;
use App\Models\Pengeluaran;
use DB;
use Cache;

/** 
 * @Author: fajar 
 * @Date: 2018-03-27 16:12:45 
 * @Desc:  
 */

class PengeluaranBaseLogic 
{

    protected $pengeluaran;

    public function __construct(Pengeluaran $pengeluaran)
    {
        $this->pengeluaran = $pengeluaran;
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:12:52 
     * @Desc: get data for sum pengeluaran 
     */    
    public function setPengeluaran()
    {
        try {

            $data = DB::connection('tabungan')->table('pengeluaran')->select('*')->where('cif_id',\Auth::user()->id);
            $pengeluaran = $data->get();
            $pengeluaranku=0;
            foreach($pengeluaran as $row){
                $pengeluaranku += $row->nominal;
            }
            $customMeta = [
                'totalPengeluaran' => number_format($pengeluaranku, 2,',','.'),
                'currency'  =>'IDR',

            ];
            return $customMeta;
        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:13:30 
     * @Desc: get data list all pengeluaran  
     */    
    public function setListPengeluaran()
    {
        try {
            $data = DB::connection('tabungan')->table('pengeluaran')->select('*')->where('cif_id',\Auth::user()->id)->orderBy('id','DESC')->get();
            
            return $data;
        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:14:08 
     * @Desc: post process data pengeluaran 
     */    
    public function setCreatePengeluaran($request)
    {
        try {

            $pengeluaran = $this->pengeluaran->newInstance();
            $pengeluaran->cif_id = \Auth::user()->id;
            $pengeluaran->name = $request['name'];
            $pengeluaran->description = $request['description'];
            $pengeluaran->nominal = $request['nominal'];
            $pengeluaran->created_at = \Carbon\Carbon::now();
            $pengeluaran->save();
            return $pengeluaran;
        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:14:37 
     * @Desc: delete data pengeluaran by id 
     */    
    public function deletePengeluaran($id){
        try {
            $pengeluaran=$this->pengeluaran->find($id)->delete();
            return $pengeluaran;
        } catch(\Exception $e) {
            return false;
        }
    }
}
