<?php
namespace App\Modules\Pengeluaran;

use Illuminate\Http\Request;
use App\Modules\Pengeluaran\PengeluaranBaseLogic as baseLogic;

use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use App\Modules\Common\Exception\ValidationFailException;

use App\Modules\Common\Response\ApiResponse;
use App\Modules\Common\Constant\ApiConstant;
use App\Modules\Common\Constant\HttpConstant as HTTP;

class PengeluaranLogic
{
    protected $baseLogic;
    protected $response;
    function __construct(baseLogic $baseLogic, ApiResponse $response)
    {
        $this->baseLogic = $baseLogic;
        $this->response = $response;
    }

    public function getPengeluaran()
    {
        $Pengeluaran = $this->baseLogic->setPengeluaran();
        return $Pengeluaran;
        // try
        // {
        //     $Pengeluaran = $this->baseLogic->setPengeluaran();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($Pengeluaran);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getListPengeluaran()
    {
        $ListPengeluaran = $this->baseLogic->setListPengeluaran();
        return $ListPengeluaran;
        // try
        // {
        //     $ListPengeluaran = $this->baseLogic->setListPengeluaran();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($ListPengeluaran);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getCreatePengeluaran($requset)
    {
        $pengeluaran = $this->baseLogic->setCreatePengeluaran($requset);
        return $pengeluaran;
    }

    public function delete($request){
        $pengeluaran=$this->baseLogic->deletePengeluaran($request);
        return $pengeluaran;
    }
}