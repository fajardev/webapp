<?php
  
Route::group(['namespace' => 'Tabungan', 'prefix' => 'tabungan','middleware' => 'auth'], function () {
	
	Route::get('/','TabunganController@Tabungan');
	Route::get('/create','TabunganController@create');
	Route::post('/proses','TabunganController@store');
	// Route::get('/edit/{id}','BukuController@edit');
	// Route::post('/update/{id}','BukuController@update');
	Route::get('/delete/{id}','TabunganController@delete');
	
});

?>
