<?php
namespace App\Modules\Tabungan;

use App\Modules\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Modules\Tabungan\TabunganLogic as Tabungan;
use App\Modules\Tabungan\TabunganConstant as Message;
use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use Session;

class TabunganController extends BaseController
{
    protected $tabungan;
    protected $request;
    function __construct(
        Tabungan $tabungan, 
        Request $request
    )
    {
        $this->tabungan = $tabungan;
        $this->request = $request;
    }

    public function Tabungan()
    {
        $req = $this->request->all();

        try{

            $response['tabungan'] = $this->tabungan->getTabungan($req);
            $response['list'] = $this->tabungan->getListTabungan($req);
            $response['title'] = 'Tabungan Anda.';
            return response()->view('Tabungan.data_tabungan',$response);
        
        } catch(\Exception $e) {
            return response()->view('errors');
        }
        //return response()->json($response->transform(), $response->httpCd())->header('Access-Control-Allow-Origin', '*');
    }

    public function ListTabungan()
    {
        $req = $this->request->all();
        $response = $this->tabungan->getListTabungan($req);
        return response()->json($response->transform(), $response->httpCd())->header('Access-Control-Allow-Origin', '*');
    }

    public function create(){
        try{
            $data['title']=Message::CREATE_NEW_TABUNGAN_MSG;
            return response()->view('Tabungan.create_tabungan',$data);
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }

    public function store(){
        $request=$this->request->all();
        try{
            $data=$this->tabungan->getCreateTabungan($request);
            Session::flash('message',Message::CREATE_NEW_TABUNGAN_SUCCESS_MSG);
            return redirect('tabungan/create');
        } catch(\Exception $e) {
            return response()->view('errors');
        }
        
    }

    public function delete($id){
        try{
            $buku=$this->tabungan->delete($id);
            Session::flash('message', Message::DELETE_TABUNGAN_SUCCESS);
            return redirect('/tabungan');
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }
}