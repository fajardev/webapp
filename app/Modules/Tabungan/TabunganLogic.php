<?php
namespace App\Modules\Tabungan;

use Illuminate\Http\Request;
use App\Modules\Tabungan\TabunganBaseLogic as baseLogic;

use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use App\Modules\Common\Exception\ValidationFailException;

use App\Modules\Common\Response\ApiResponse;
use App\Modules\Common\Constant\ApiConstant;
use App\Modules\Common\Constant\HttpConstant as HTTP;

class TabunganLogic
{
    protected $baseLogic;
    protected $response;
    function __construct(baseLogic $baseLogic, ApiResponse $response)
    {
        $this->baseLogic = $baseLogic;
        $this->response = $response;
    }
    
    public function getTabungan(){
        $tabungan = $this->baseLogic->setTabungan();
        return $tabungan;
        // try
        // {
        //     $tabungan = $this->baseLogic->setTabungan();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($tabungan);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getListTabungan()
    {
        $Listtabungan = $this->baseLogic->setListTabungan();
        return $Listtabungan;
        // try
        // {
        //     $Listtabungan = $this->baseLogic->setListTabungan();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($Listtabungan);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getCreateTabungan($request)
    {
        $createTabungan = $this->baseLogic->setCreateTabungan($request);
        return $createTabungan;
    }

    public function delete($request){
        $tabungan=$this->baseLogic->deleteTabungan($request);
        return $tabungan;
    }
}