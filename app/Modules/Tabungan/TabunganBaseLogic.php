<?php
namespace App\Modules\Tabungan;

use Illuminate\Http\Request;
use App\Models\Tabungan;
use App\Modules\Transformers\TabunganTransformer;
use ValidateRequests;
use DB;
use Cache;

class TabunganBaseLogic
{
   protected $tabungan;

   function __construct(Tabungan $tabungan)
   {
       $this->tabungan = $tabungan;
   }

    public function setTabungan()
    {
        try{

            $data = DB::connection('tabungan')->table('tabungan_save')->select('*')->where('cif_id',\Auth::user()->id);
            $tabungan = $data->get();
            $tabunganku=0;
            foreach($tabungan as $row){
                $tabunganku += $row->nominal_save;
            }
            $customMeta = [
                'totalTabungan' => number_format($tabunganku, 2,',','.'),
                'currency'  =>'IDR',

            ];
            return $customMeta;
        
        } catch(\Exception $e) {
            return false;
        }
    }

    public function setListTabungan()
    {
        try{

            $data = DB::connection('tabungan')->table('tabungan_save')->select('*')->where('cif_id',\Auth::user()->id)->orderBy('save_id','DESC')->get();
            return $data;
        
        } catch(\Exception $e) {
            return false;
        }
    }

    public function setCreateTabungan($request)
    {
        try{
         
            $tabungan = $this->tabungan->newInstance();
            $tabungan->cif_id = \Auth::user()->id;
            $tabungan->name = $request['name'];
            $tabungan->description = $request['description'];
            $tabungan->nominal_save = $request['nominal_save'];
            $tabungan->created_at = \Carbon\Carbon::now();
            $tabungan->save();
            return $tabungan;
           
        } catch(\Exception $e) {
            return false;
        }
    }

    public function deleteTabungan($id){
        try{
            $tabungan=$this->tabungan->find($id)->delete();
            return $tabungan;
        } catch(\Exception $e) {
            return false;
        }
    }
}