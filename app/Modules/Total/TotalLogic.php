<?php
namespace App\Modules\Total;

use Illuminate\Http\Request;
use App\Modules\Total\TotalBaseLogic as baseLogic;

use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use App\Modules\Common\Exception\ValidationFailException;

use App\Modules\Common\Response\ApiResponse;
use App\Modules\Common\Constant\ApiConstant;
use App\Modules\Common\Constant\HttpConstant as HTTP;

class TotalLogic
{
    protected $baseLogic;
    protected $response;
    function __construct(baseLogic $baseLogic, ApiResponse $response)
    {
        $this->baseLogic = $baseLogic;
        $this->response = $response;
    }

    public function getTotal()
    {
        $total = $this->baseLogic->setTotal();
        return $total;
    }
}