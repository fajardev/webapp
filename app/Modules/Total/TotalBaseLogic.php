<?php
namespace App\Modules\Total;

use Illuminate\Http\Request;
use ValidateRequests;
use DB;
use Cache;

class TotalBaseLogic
{
   
    public function setTotal()
    {
        
        $pemasukan = DB::connection('tabungan')->table('pemasukan')->select('nominal')->where('cif_id', \Auth::user()->id)->sum('nominal');
        $pengeluaran = DB::connection('tabungan')->table('pengeluaran')->select('nominal')->where('cif_id', \Auth::user()->id)->sum('nominal');
        #$tabungan = DB::connection('tabungan')->table('tabungan_save')->select('nominal_save')->sum('nominal_save');

        $data = [
            'totalUang' => number_format($pemasukan - $pengeluaran , 2,',','.'),
            'currency'  =>'IDR',

        ];
        return $data;
    }
}