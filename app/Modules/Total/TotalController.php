<?php  
namespace App\Modules\Total;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Total\TotalLogic as Total;
use App\Modules\Home\HomeConstant as Message;
use Session;

class TotalController extends BaseController{

	protected $total;
    protected $request;
    
	public function __construct(
		Total $total, 
		Request $request
	)
	{
		$this->total = $total;
        $this->request = $request;
		//$this->middleware('guest');
	}

	public function Total()
	{
		$req = $this->request->all();
		$data['title'] = Message::TITLE_HOME;
        $data['total'] = $this->total->getTotal();
		return response()->view('home',$data);
	}
}

?>