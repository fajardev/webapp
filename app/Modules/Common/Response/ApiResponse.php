<?php

namespace App\Modules\Common\Response;

use App\Modules\Common\Constant\ApiConstant;

class ApiResponse implements ResponseTransformer {

    private $status;

    private $data;

    private $messageCd;

    private $message;

    private $httpCd;

    private $detail;

    public function status() {
        return $this->status;
    }
    public function setStatus($status) {
        $this->status = $status;
    }
    public function data() {
        return $this->data;
    }
    public function setData($data) {
        $this->data = $data;
    }
    public function messageCd(){
        return $this->messageCd;
    }
    public function setMessageCd($messageCd) {
        $this->messageCd = $messageCd;
    }
    public function message(){
        return $this->message;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function httpCd(){
        return $this->httpCd;
    }
    public function setHttpCd($httpCd) {
        $this->httpCd = $httpCd;
    }
    public function detail(){
        return $this->detail;
    }
    public function setDetail($detail) {
        $this->detail = $detail;
    }

    public function transform() {

        $res = [
            "success" => $this->status,
            "body" => $this->data
        ];

        if(ApiConstant::ERROR_CD === $this->status) {

            $message = [
                "code" => $this->messageCd,
                "error" => 'Tidak Dapat di Akses Sementara Waktu.',
                "description" => $this->message,
                "detail" => $this->detail,
                
            ];

            $res["message"][] = $message;
        }
        return $res;
    }
}