<?php

namespace App\Modules\Common\Response;

interface ResponseTransformer {

    public function transform();
}