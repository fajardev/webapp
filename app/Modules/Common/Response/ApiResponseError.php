<?php
namespace App\Modules\Common\Response;

use App\Modules\Common\Constant\ApiConstant;

class ApiResponseError extends ApiResponse implements ResponseTransformer{

    public function transform() {
        if (parent::status() == ApiConstant::SUCCESS_CD) {
            $res['success'] = TRUE;
            $res['data'] = parent::data();
        }else {
            $res['success'] = FALSE;
            $res['message'] = parent::message();
        }
        return $res;
    }
}