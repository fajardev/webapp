<?php
namespace App\Modules\Common\Constant;

class ApiConstant {

    const SUCCESS_CD = true;
    const SUCCESS_MSG = 'Request completed';

    const ERROR_CD = false;
    const ERROR_MSG = 'Request cannot be completed';
}