<?php

namespace App\Modules\Common\Logic;

use Illuminate\Support\Facades\Config;

class CannedOutputLogic
{
    protected $cannedOutputList = array();

    public function __construct()
    {
        $this->cannedOutputList = Config::get('cannedOutput');
    }

    public function get($code, $additionalInfo = array())
    {
        $output = array();
        if (isset($this->cannedOutputList[$code]['httpcode'])) {
            $output['httpcode'] = $this->cannedOutputList[$code]['httpcode'];
        }
        if (isset($this->cannedOutputList[$code]['metadata'])) {
            $output['metadata'] = $this->cannedOutputList[$code]['metadata'];
        }
        if (isset($this->cannedOutputList[$code]['header'])) {
            $output['header'] = $this->cannedOutputList[$code]['header'];
        }
        if (isset($this->cannedOutputList[$code]['body'])) {
            $output['body'] = $this->cannedOutputList[$code]['body'];
        }

        if ($additionalInfo !== null) {
            foreach ($additionalInfo as $key => $value) {
                $output['body'][$key] = $value;
            }
        } else {
            $output['body'] = null;
        }

        return $output;
    }
}