<?php

namespace App\Modules\Common\Logic;
use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Logic\CannedOutputLogic;
use Illuminate\Support\Facades\Config;

class IabeeLogic
{
    protected $cannedOutput;

    public function __construct()
    {
        $this->cannedOutput = new CannedOutputLogic();
    }

    public function validateRequest($validator) {
        if ($validator->fails()) {
            $e = new InvalidInputException();
            $e->setAdditionalMessage(array('errors' => $validator->errors()->toArray()));
            throw $e;
        }
    }
}