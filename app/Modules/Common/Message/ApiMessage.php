<?php
namespace App\Modules\Common\Message;

class ApiMessage {

    const FATAL_ERROR_CD = 'ERR-API-001';
    const FATAL_ERROR_MSG = 'Request cannot be completed';

    const INVALID_REQUEST_FORMAT_CD = 'ERR-API-002';
    const INVALID_REQUEST_FORMAT_MSG = 'Invalid request format';

    const VALIDATION_FAIL_CD = 'ERR-API-003';
    const VALIDATION_FAIL_MSG = 'Validation error';

    const FORBIDDEN_CD = 'ERR-API-004';
    const FORBIDDEN_MSG = 'Forbidden error';

    const INVALID_JWT_CD = 'ERR-API-005';
    const INVALID_JWT_MSG = 'Invalid access token';

    const INVALID_ACTIVE_ROLE_CD = 'ERR-API-006';
    const INVALID_ACTIVE_ROLE_MSG = 'Invalid active role';

    const TEMPLATE_NOT_FOUND_CD = 'ERR-API-007';
    const TEMPLATE_NOT_FOUND_MSG = 'Template Not Found';
}
