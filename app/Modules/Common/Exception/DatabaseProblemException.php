<?php

namespace App\Modules\Common\Exception;

use App\Modules\Common\Exception\BukuException;
use App\Modules\Common\Message\ApiMessage as Message;


class DatabaseProblemException extends BukuException
{

    public function __construct($code = 0, Exception $previous = null) {

        $this->code = Message::FATAL_ERROR_CD;
        $this->message = Message::FATAL_ERROR_MSG;

        parent::__construct($this->message, $code, $previous);
    }

    public function getBukuOutput()
    {
        return $this->cannedOutput->get('database_problem');
    }
}