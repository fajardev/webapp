<?php

namespace App\Modules\Common\Exception;

class ValidationFailException extends \Exception{

    public function __construct($messageCode, $message, $messageBag = null, $code = 0, Exception $previous = null) {

        $this->code = $messageCode;
        $this->message = $message;


        parent::__construct($message, $code, $previous);

        $this->messageBag = $messageBag;
    }

    public function getMessageBag() {
        return $this->messageBag;
    }
}