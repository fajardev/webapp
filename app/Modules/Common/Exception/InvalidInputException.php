<?php

namespace App\Modules\Common\Exception;

use App\Modules\Common\Exception\BukuException;
use App\Modules\Common\Message\ApiMessage as Message;

class InvalidInputException extends IabeeException
{

    public function __construct($code = 0, Exception $previous = null) {

        $this->code = Message::INVALID_REQUEST_FORMAT_CD;
        $this->message = Message::INVALID_REQUEST_FORMAT_MSG;

        parent::__construct($this->message, $code, $previous);
    }

    public function getIabeeOutput()
    {
        //TODO: build some kind of builder
        $output = array(
            'httpcode' => 400,
            'header' => [
                'WWW-Authenticate' => ['value' => 'Basic']
            ],
            'metadata' => [
                'success' => 0,
                'message' => [
                    ["code" => "E_GENERAL_INVALID_INPUT", "value" => 'Invalid Input'],
                    $this->message
                ],
            ]
        );

        return $output;
    }
}