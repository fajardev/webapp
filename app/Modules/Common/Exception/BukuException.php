<?php

namespace App\Modules\Common\Exception;
use App\Modules\Common\Logic\CannedOutputLogic;
use Illuminate\Support\Facades\Config;

abstract class IabeeException extends \Exception
{
    protected $cannedOutput;
    protected $message;

    public function __construct()
    {
        $this->cannedOutput = new CannedOutputLogic();
    }

    abstract public function getBukuOutput();

    public function setAdditionalMessage($message) {
        $this->message = $message;
    }
}