<?php  
namespace App\Modules\Authentication;

use Illuminate\Http\Request;
use App\Modules\Authentication\AuthenticationBaseLogic as BaseLogic;

	class AuthenticationLogic{
		protected $baseLogic;

		public function __construct(BaseLogic $baseLogic){
			$this->baseLogic=$baseLogic;
		}

		public function AuthenticationUser($request){
			$userAuth=$this->baseLogic->getAuthUser($request);
			return $userAuth;
		}

	}
?>