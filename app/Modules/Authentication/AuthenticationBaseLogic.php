<?php  
namespace App\Modules\Authentication;

use Illuminate\Http\Request;
use App\Models\UserModel;

class AuthenticationBaseLogic{

	protected $userAuth;

	public function __construct(UserModel $userAuth){
		$this->userAuth=$userAuth;
	}

	public function getAuthUser($username,$password){
		$userAuth=$this->userAuth->find($username,$password);
		return $userAuth;
	}
}
?>