<?php  
namespace App\Modules\Authentication;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Authentication\AuthenticationConstant as Message;
use App\Modules\Authentication\AuthenticationLogic;

class AuthenticationController extends BaseController{

	protected $request;
	protected $authLogic;

	public function __construct(Request $request, AuthenticationLogic $authLogic){
		$this->request 		= $request;
		$this->authLogic 	= $authLogic;
	}

	public function index(){
		
	}
}

?>