<?php  
namespace App\Modules\Buku;

use Illuminate\Http\Request;
use App\Modules\Buku\BukuBaseLogic as BaseLogic;
use Session;

	class BukuLogic{

		protected $baseLogic;
		public function __construct(BaseLogic $baseLogic){
			$this->baseLogic=$baseLogic;
		}

		public function getAll(){
			$buku=$this->baseLogic->getAllBuku();
			return $buku;
		}

		public function create($request){
			$buku=$this->baseLogic->createReviewBuku($request);
			return $buku;
		}

		public function edit($request){
			$buku=$this->baseLogic->editReviewBuku($request);
			return $buku;
		}

		public function update($id,$request){
			$buku=$this->baseLogic->updateReviewBuku($id,$request);
			return $buku;
		}

		public function delete($request){
			$buku=$this->baseLogic->deleteReviewBuku($request);
			return $buku;
		}
	}

?>