<?php  
Route::group(['namespace' => 'Buku', 'prefix' => 'review-buku','middleware' => 'auth'], function () {
	
	Route::get('/','BukuController@index');
	Route::get('/create','BukuController@create');
	Route::post('/proses','BukuController@store');
	Route::get('/edit/{id}','BukuController@edit');
	Route::post('/update/{id}','BukuController@update');
	Route::get('/delete/{id}','BukuController@delete');
	
});

?>