<?php  
namespace App\Modules\Buku;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Buku\BukuConstant as Message;
use Session;
use Auth;


	class BukuController extends BaseController{

		protected $bukuLogic;
		protected $request;
		public function __construct(BukuLogic $bukuLogic, Request $request){
			$this->middleware('auth');
			$this->bukuLogic=$bukuLogic;
			$this->request=$request;
		}

		/**
		 * [index description]
		 * @return [type] [description]
		 */
		public function index(){
			$data['title']=Message::BUKU_TITLE;
			$request=$this->request->all();
			$data['buku']=$this->bukuLogic->getAll($request);
	
			//return response()->json($data);
			return response()->view('Admin.Buku.data_buku',$data);
		}

		/**
		 *  
		 * @return [type] [description]
		 */
		public function create(){
			$data['title']=Message::CREATE_NEW_REVIEW_BUKU_MSG;
			return response()->view('Admin.Buku.create_buku',$data);
		}

		public function store(){
			$request=$this->request->all();
			$data=$this->bukuLogic->create($request);
			Session::flash('message',Message::CREATE_NEW_REVIEW_BUKU_SUCCESS_MSG);
			return redirect('review-buku/create');
		}

		public function edit($id){
			$data['title']=Message::EDIT_REVIEW_BUKU;
			$data['reg']=$this->bukuLogic->edit($id);
			return response()->view('Admin.Buku.edit_buku',$data);
		}

		public function update($id){
			$request=$this->request->all();
			$buku=$this->bukuLogic->update($id,$request);
			return redirect('/review-buku');
		}

		public function delete($id){
			$buku=$this->bukuLogic->delete($id);
	        Session::flash('message', Message::DELETE_REVIEW_BUKU_SUCCESS);
	        return redirect('/review-buku');
		}

	}
?>