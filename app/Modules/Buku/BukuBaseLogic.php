<?php  
namespace App\Modules\Buku;

use Illuminate\Http\Request;
use App\Models\BukuModel;
use Illuminate\Support\Facades\Input;
use Session;

	class BukuBaseLogic{
		protected $buku;

		public function __construct(BukuModel $buku){
			$this->buku=$buku;
		}

		public function getAllBuku(){
			$buku=$this->buku->all();
			return $buku;
		}

		public function createReviewBuku($request){
			$buku=$this->buku->newInstance();
			$buku->judul=$request['judul'];
			$buku->penulis=$request['penulis'];
			$buku->penerbit=$request['penerbit'];
			$buku->tgl_terbit=$request['tgl_terbit'];
			$buku->isi=$request['isi'];
			$buku->tebel_buku=$request['tebel_buku'];
			$buku->dimensi=$request['dimensi'];
			$buku->harga=$request['harga'];
			if(Input::hasFile('image')) {
	            $file = Input::file('image');
	            $name = time(). '-' .$file->getClientOriginalName();
	            $buku->filePath = $name;
	            $file->move(public_path().'/images/', $name);
	        }
			$buku->save();
			return $buku;
		}

		public function editReviewBuku($id){
			$buku=$this->buku->find($id);
			return $buku;
		}

		public function updateReviewBuku($id,$request){
			$buku=$this->buku->find($id);
			$buku->judul=$request['judul'];
			$buku->penulis=$request['penulis'];
			$buku->penerbit=$request['penerbit'];
			$buku->tgl_terbit=$request['tgl_terbit'];
			$buku->isi=$request['isi'];
			$buku->tebel_buku=$request['tebel_buku'];
			$buku->dimensi=$request['dimensi'];
			$buku->harga=$request['harga'];
			if(Input::hasFile('image')) {
	            $file = Input::file('image');
	            $name = time(). '-' .$file->getClientOriginalName();
	            $buku->filePath = $name;
	            $file->move(public_path().'/images/', $name);
	        }
			$buku->save();
			return $buku;
		}

		public function deleteReviewBuku($id){
			$buku=$this->buku->find($id)->delete();
			return $buku;
		}
	}

?>