<?php

Route::group(['namespace' => 'Pemasukan', 'prefix' => 'pemasukan','middleware' => 'auth'], function () 
{    
    Route::get('/','PemasukanController@Pemasukan');
    Route::get('/create', 'PemasukanController@create');
    Route::post('/proses', 'PemasukanController@store');
    Route::get('/delete/{id}', 'PemasukanController@delete');
});