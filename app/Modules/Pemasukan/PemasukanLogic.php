<?php
namespace App\Modules\Pemasukan;

use Illuminate\Http\Request;
use App\Modules\Pemasukan\PemasukanBaseLogic as baseLogic;

use App\Modules\Common\Exception\InvalidInputException;
use App\Modules\Common\Exception\DatabaseProblemException;
use App\Modules\Common\Exception\ValidationFailException;

use App\Modules\Common\Response\ApiResponse;
use App\Modules\Common\Constant\ApiConstant;
use App\Modules\Common\Constant\HttpConstant as HTTP;

class PemasukanLogic
{
    protected $baseLogic;
    protected $response;
    function __construct(baseLogic $baseLogic, ApiResponse $response)
    {
        $this->baseLogic = $baseLogic;
        $this->response = $response;
    }

    public function getPemasukan(){
        $Pemasukan = $this->baseLogic->setPemasukan();
        return $Pemasukan;
        // try
        // {
        //     $Pemasukan = $this->baseLogic->setPemasukan();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($Pemasukan);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getListPemasukan(){
        $ListPemasukan = $this->baseLogic->setListPemasukan();
        return $ListPemasukan;
        // try
        // {
        //     $ListPemasukan = $this->baseLogic->setListPemasukan();
        //     $this->response->setStatus(ApiConstant::SUCCESS_CD);
        //     $this->response->setData($ListPemasukan);
        //     $this->response->setHttpCd(HTTP::HTTP_OK);
        // }
        // catch(DatabaseProblemException $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // catch(\Exception $e)
        // {
        //     $this->response->setStatus(ApiConstant::ERROR_CD);
        //     $this->response->setMessageCd($e->getCode());
        //     $this->response->setMessage($e->getMessage());
        //     $this->response->setHttpCd(HTTP::HTTP_INTERNAL_SERVER_ERROR);
        // }
        // return $this->response;
    }

    public function getCreatePemasukan($request)
    {
        $pemasukan = $this->baseLogic->setCreatePemasukan($request);
        return $pemasukan;
    }

    public function delete($request){
        $pemasukan=$this->baseLogic->deletePemasukan($request);
        return $pemasukan;
    }
}