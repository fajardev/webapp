<?php
namespace App\Modules\Pemasukan;

use Illuminate\Http\Request;
use App\Modules\Transformers\PemasukanTransformer;
use App\Models\Pemasukan;
use DB;
use Cache;

/** 
 * @Author: fajar 
 * @Date: 2018-03-27 16:15:09 
 * @Desc:  
 */

class PemasukanBaseLogic 
{

    protected $pemasukan;

    public function __construct(Pemasukan $pemasukan)
    {
        $this->pemasukan = $pemasukan;
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:15:20 
     * @Desc: get sum data pemasukan 
     */    
    public function setPemasukan()
    {
        try {

            $data = DB::connection('tabungan')->table('pemasukan')->select('*')->where('cif_id',\Auth::user()->id);
            $pemasukan = $data->get();
            $pemasukanku = 0;
            $pengeluaran = DB::connection('tabungan')->table('pengeluaran')->select('nominal')->where('cif_id',\Auth::user()->id)->sum('nominal');
            foreach($pemasukan as $row){
                $pemasukanku += $row->nominal;
            }
            $customMeta = [
                'totalpemasukan' => number_format($pemasukanku - $pengeluaran, 2,',','.'),
                'currency'  =>'IDR',

            ];
            return $customMeta;

        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:16:01 
     * @Desc: get list all pemasukan 
     */    
    public function setListPemasukan()
    {
        try {

            $data = DB::connection('tabungan')->table('pemasukan')->select('*')->where('cif_id',\Auth::user()->id)->orderBy('id','DESC')->get();
            
            return $data;
        
        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:16:15 
     * @Desc: post process data pemasukan 
     */    
    public function setCreatePemasukan($request)
    {
        try {

            $pemasukan = $this->pemasukan->newInstance();
            $pemasukan->cif_id = \Auth::user()->id;
            $pemasukan->name = $request['name'];
            $pemasukan->description = $request['description'];
            $pemasukan->nominal = $request['nominal'];
            $pemasukan->created_at = \Carbon\Carbon::now();
            $pemasukan->save();
            return $pemasukan;
        
        } catch(\Exception $e) {
            return false;
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:16:35 
     * @Desc: delete pemasukan by id 
     */    
    public function deletePemasukan($id){
        try {

            $pemasukan=$this->pemasukan->find($id)->delete();
            return $pemasukan;
        
        } catch(\Exception $e) {
            return false;
        }
    }
}