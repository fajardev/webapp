<?php
namespace App\Modules\Pemasukan;

use App\Modules\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Modules\Pemasukan\PemasukanLogic as Pemasukan;
use App\Modules\Pemasukan\PemasukanConstant as Message;
use Session;

/** 
 * @Author: fajar 
 * @Date: 2018-03-27 16:18:50 
 * @Desc:  
 */
class PemasukanController extends BaseController
{
    protected $pemasukan;
    protected $request;
    function __construct(
        Pemasukan $pemasukan, 
        Request $request
    )
    {
        $this->pemasukan = $pemasukan;
        $this->request = $request;
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:18:55 
     * @Desc: page list of data pemasukan and sum pemasukan 
     */    
    public function Pemasukan()
    {
        try {

            $req = $this->request->all();
            $response['pemasukan'] = $this->pemasukan->getPemasukan($req);
            $response['list'] = $this->pemasukan->getListPemasukan($req);
            $response['title'] = 'Pemasukan Anda.';
            return response()->view('Pemasukan.data_pemasukan',$response);
        
        } catch(\Exception $e) {
            return response()->view('errors');
        }
        
    }

    public function ListPemasukan()
    {
        $req = $this->request->all();
        $response = $this->pemasukan->getListPemasukan($req);
        return response()->json($response->transform(), $response->httpCd())->header('Access-Control-Allow-Origin', '*');
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:20:42 
     * @Desc: page input data pemasukan 
     */    
    public function create()
    {
        try {

            $res['title'] = Message::CREATE_NEW_PEMASUKAN_MSG;
            return response()->view('Pemasukan.create_pemasukan',$res);
        
        } catch(\Exception $e) {
            return response()->view('errors');
        }
        
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:21:41 
     * @Desc: post process of data pemasukan
     */    
    public function store()
    {
        try {

            $request=$this->request->all();
            $data=$this->pemasukan->getCreatePemasukan($request);
            Session::flash('message',Message::CREATE_NEW_PEMASUKAN_SUCCESS_MSG);
            return redirect('pemasukan/create');

        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:22:35 
     * @Desc: delete pemasukan by id 
     */    
    public function delete($id){
        try {

            $pemasukan=$this->pemasukan->delete($id);
            Session::flash('message', Message::DELETE_PEMASUKAN_SUCCESS);
            return redirect('/pemasukan');
        
        } catch(\Exception $e) {
            return response()->view('errors');
        }
    }
}