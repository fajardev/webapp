<?php

namespace App\Modules\Transformers;

use League\Fractal;

class PemasukanTransformer extends Fractal\TransformerAbstract
{
    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:28:29 
     * @Desc: transform to pemasukan 
     */    
    public function transform($product)
    {
        return [
            'id' => (int) $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'nominal' => number_format($product->nominal,2,',','.'),
            'created' => $product->created_at,
        ];
    }
}