<?php

namespace App\Modules\Transformers;

use League\Fractal;

class TabunganTransformer extends Fractal\TransformerAbstract
{

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:29:10 
     * @Desc: transform to tabungan 
     */    
    public function transform($product)
    {
        return [
            'id' => (int) $product->save_id,
            'name' => $product->name,
            'description' => $product->description,
            'nominal' => number_format($product->nominal_save,2,',','.'),
            'created' => $product->created_at,
            'updated' => $product->updated_at,
        ];
    }
}