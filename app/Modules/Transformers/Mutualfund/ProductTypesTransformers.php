<?php

namespace App\Modules\Transformers\Mutualfund;

use League\Fractal;

class ProductTypesTransformers extends Fractal\TransformerAbstract
{
    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:28:00 
     * @Desc: transform to products type 
     */    
    public function transform($product)
    {
        return [
            'id' => (int) $product->id,
            'name' => $product->name == 'Money Market' ? 'Pasar Uang' : ($product->name == 'Fixed Income' ? 'Pendapatan Tetap' : ($product->name == 'Equity Fund' ? 'Saham':($product->name == 'Balanced Fund' ? 'Campuran' : 'Index & ETF'))),
        ];
    }
}