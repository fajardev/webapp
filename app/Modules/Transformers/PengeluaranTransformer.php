<?php

namespace App\Modules\Transformers;

use League\Fractal;

class PengeluaranTransformer extends Fractal\TransformerAbstract
{

    /** 
     * @Author: fajar 
     * @Date: 2018-03-27 16:28:54 
     * @Desc: transform to pengeluaran 
     */    
    public function transform($product)
    {
        return [
            'id' => (int) $product->id,
            'name' => $product->name,
            'description' => $product->description,
            'nominal' => number_format($product->nominal,2,',','.'),
            'created' => $product->created_at,
        ];
    }
}