<?php

namespace App\Modules\User;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\User\UserConstant as Message;
use Session;
use Auth;

class UserController extends BaseController{
    protected $userLogic;
    protected $request;
    public function __construct(Request $request,UserLogic $userLogic){
        $this->middleware('auth');
        $this->request=$request;
        $this->userLogic=$userLogic;
        
    }
    /**
     * [index description]
     * [page index]
     * @return [type] [description]
     */
    public function index(){
        $request=$this->request->all();
        $data=$this->userLogic->getAll($request);
        $data=Message::TITLE;
        return response()->view('Admin.Users.data_user',$data);

    }

    /**
     * [create description]
     * [page input user]
     * @return [type] [description]
     */
    public function create(){
    	$data['title']=Message::CREATE_TITLE;
        return response()->view('Admin.Users.create_user',$data);
    }

    /**
     * [store description]
     * [processing input user]
     * @return [type] [description]
     */
    public function store(){
        $request=$this->request->all();
        $data=$this->userLogic->create($request);
        Session::flash('message',Message::CREATE_USER_SUCCESS);
        return redirect('user/create');
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id){
        $data['title']=Message::EDIT_TITLE;
        $data['dataEdit']=$this->userLogic->edit($id);
        return response()->view('Admin.Users.edit_user',$data);
    }

    /**
     * [update description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function update($id){
        $request=$this->request->all();
        $user=$this->userLogic->updateUser($id,$request);
        Session::flash('message',Message::EDIT_USER_SUCCESS);
        return redirect('/user');
    }

    /**
     * [delete description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function delete($id){
        $user=$this->userLogic->delete($id);
        Session::flash('message',Message::DELETE_USER_SUCCESS);
        return redirect('/user');
    }

}
