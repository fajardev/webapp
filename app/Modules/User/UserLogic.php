<?php  
namespace App\Modules\User;

use Illuminate\Http\Request;
use App\Modules\User\UserBaseLogic as BaseLogic;

class UserLogic{
	protected $baseLogic;

	public function __construct(BaseLogic $baseLogic){
		$this->baseLogic=$baseLogic;
	}

	/**
	 * [getAll description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getAll($request){
		$user=$this->baseLogic->getAllUser($request);
		return $user;
	}

	/**
	 * [create description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function create($request){
		$user=$this->baseLogic->createUser($request);
		return $user;
	}

	/**
	 * [edit description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function edit($request){
		$user=$this->baseLogic->getEditUser($request);
		return $user;
	}

	public function updateUser($id,$request){
		$user=$this->baseLogic->getUpdate($id,$request);
		return $user;
	}

	/**
	 * [delete description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function delete($request){
		$user=$this->baseLogic->getDeleteUser($request);
		return $user;
	}
}

?>