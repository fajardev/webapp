<?php  

Route::group(['namespace' => 'User', 'prefix' => 'user','middleware' => 'auth'], function () {
	//Route::group(['middleware' => 'auth'], function () {
	    Route::get('/users','UserController@index');
	    Route::get('/create','UserController@create');
	    Route::post('/proses','UserController@store');
	    Route::get('/edit/{id}','UserController@edit');
	    Route::post('/update/{id}','UserController@update');
	    Route::get('/delete/{id}','UserController@delete');
	//});
});
?>