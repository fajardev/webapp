<?php  
namespace App\Modules\User;

use Illuminate\Http\Request;
use App\Models\UserModel;

class UserBaseLogic{
	protected $user;

	public function __construct(UserModel $user){
		$this->user=$user;
	}

	/**
	 * [getAllUser description]
	 * @return [type] [description]
	 */
	public function getAllUser(){
		$user=$this->user->all();
		return $user;
	}

	/**
	 * [createUser description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function createUser($request){
		$user=$this->user->newInstance();
		$user->name=$request['name'];
		$user->username=$request['username'];
		$user->email=$request['email'];
		$user->password=\Hash::make($request['password']);
		$user->save();

		return $user;
	}

	/**
	 * [getEditUser description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getEditUser($id){
		$user=$this->user->find($id);
		return $user;
	}

	public function getUpdate($id,$request){
		$user=$this->user->find($id);
		$user->name=$request['name'];
		$user->username=$request['username'];
		$user->email=$request['email'];
		$user->save();
		return $user;
	}

	/**
	 * [getDeleteUser description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getDeleteUser($id){
		$user=$this->user->find($id)->delete();
		return $user;
	}
}


?>