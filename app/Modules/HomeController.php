<?php  
namespace App\Modules;

use Illuminate\Http\Request;
use App\Modules\Controllers\BaseController;
use App\Modules\Home\HomeConstant as Message;

class HomeController extends BaseController{

	public function __construct(){
		$this->middleware('auth');
	}

	public function index(){
		$data['title']=Message::TITLE_HOME;
		return response()->view('home',$data);
	}
}

?>