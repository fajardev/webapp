<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemasukan extends Model
{
    protected $connection = 'tabungan';
    protected $table = 'pemasukan';
    protected $primaryKey = 'id';
}