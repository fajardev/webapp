<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $connection = 'tabungan';
    protected $table = 'pengeluaran';
    protected $primaryKey = 'id';
}