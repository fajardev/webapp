<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tabungan extends Model
{
    protected $connection = 'tabungan';
    protected $table = 'tabungan_save';
    protected $primaryKey = 'save_id';
}